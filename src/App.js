import React, {Component, Fragment} from "react";
import "./App.css";
import Homepage from "./pages/homepage/Homepage";
import {Route, Switch, Redirect} from "react-router-dom";
import Header from "./components/header/Header";
import SiginAndSignup from "./pages/sign-in-and-signup/SignInAndSignup";
import {auth, createUserProfileDocument} from './firebase/firebase.utils';
import {connect} from 'react-redux';
import {setCurrentUser} from './redux/user/user.actions';
import {createStructuredSelector} from 'reselect';
import {selectCurrentUser} from './redux/user/user.selector';
import CheckoutPage from './pages/checkout/Checkout';


import Shop from "./pages/shop/Shop";

class App extends Component {
    unSubscribeFromAuth = null;

    componentDidMount() {
        const {setCurrentUser} = this.props;
        //console.log(collectionsArray);
        this.unSubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
            if (userAuth) {
                const userRef = await createUserProfileDocument(userAuth);
                userRef.onSnapshot(snapshot => {
                    setCurrentUser({
                        id: snapshot.id,
                        ...snapshot.data()
                    })
                });
            }
            setCurrentUser({currentUser: userAuth});
        });
    }

    componentWillUnmount() {
        this.unSubscribeFromAuth();
    }

    render() {
        const {currentUser} = this.props;
        return (
            <Fragment>
                <Header />
                <Switch>
                    <Route exact path='/' component={Homepage}/>
                    <Route path='/shop' component={Shop}/>
                    <Route exact path='/checkout' component={CheckoutPage}/>
                    <Route exact path='/signin' render={() => (currentUser && Object.keys(currentUser).length > 1) ? (<Redirect to='/' />) : (<SiginAndSignup/>)} />
                </Switch>
            </Fragment>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    currentUser : selectCurrentUser,
});

const mapDispatchToProps = dispatch => ({
    setCurrentUser: user => dispatch(setCurrentUser(user))
});

export default connect(mapStateToProps,mapDispatchToProps)(App);
