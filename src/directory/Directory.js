import React from "react";
import MenuItem from "../components/menu-item/MenuItem";
import "./Directory.scss";
import {connect} from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {selectDirectoryValue} from '../redux/directory/directory.selector';


const Directory =({directory}) => {
    return (
        <div className='directory-menu'>
            {directory.map(({id, ...otherSectionProps}) => (
                <MenuItem key={id} {...otherSectionProps} />
            ))}
        </div>
    );
};

const mapStateToProps = createStructuredSelector({
    directory : selectDirectoryValue
});

export default connect(mapStateToProps, null) (Directory);
