import React from "react";
import "./SignInAndSignup.scss";
import SignIn from "../../components/sign-in/SignIn";
import SignUp from "../../components/sign-up/SignUp";

function SignInAndSignup() {
    return (
        <div className='sign-in-and-signup'>
            <SignIn />
            <SignUp/>
        </div>
    );
}

export default SignInAndSignup;
