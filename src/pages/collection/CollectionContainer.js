/**
 * Created by Ms.Ria (rubel mia) on 12/4/2020.
 */
import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import {isCollectionsLoaded} from '../../redux/shop/shop.selector';
import WithSpinner from "../../components/with-spinner/withSpinner";
import CollectionPage from "./Collection";


const mapStateToProps = createStructuredSelector({
    isLoading: (state) => !isCollectionsLoaded(state)
});

const CollectionContainer = compose(
    connect(mapStateToProps),
    WithSpinner
)(CollectionPage);

export default CollectionContainer;
