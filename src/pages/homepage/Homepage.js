import React from "react";
import "./Homepage.scss";
import Directory from "../../directory/Directory";

function Homepage() {
  return (
    <div>
      <div className='homepage'>
        <Directory />
      </div>
    </div>
  );
}

export default Homepage;
