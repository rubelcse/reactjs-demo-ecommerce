import React, {Component} from "react";
import {Route, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {fetchCollectionsStartAsync} from '../../redux/shop/shop.actions';
import {isCollectionsLoaded} from '../../redux/shop/shop.selector';
import CollectionOverviewContainer from '../../components/collection-overview/CollectionOverviewContainer'
import CollectionContainer from '../../pages/collection/CollectionContainer';

class Shop extends Component {

    unsubscribeFromSnapshot = null;

    componentDidMount() {
        const {fetchCollectionsStartAsync} = this.props;
        fetchCollectionsStartAsync();

    }
    render() {
        const { match } = this.props;
        return (
            <div className='shop-page'>
                <Route exact path={match.path} component={CollectionOverviewContainer } />
                <Route path={`${match.path}/:collectionId`} component={CollectionContainer} />}/>
            </div>
        )
    }
}


const mapDispatchToProps = dispatch  => ({
    fetchCollectionsStartAsync : () => dispatch(fetchCollectionsStartAsync())
});


export default withRouter(connect(null, mapDispatchToProps)(Shop));
