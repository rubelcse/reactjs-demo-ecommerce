import { createSelector } from 'reselect';

const selectDirectory = (state) => state.directory;

export const selectDirectoryValue = createSelector(
    [selectDirectory],
    directory => directory.sections
);