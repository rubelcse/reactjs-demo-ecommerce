/**
 * Created by Sultan on 7/15/2020.
 */
import ShopActionTypes from './shop.types';
import {firestore, convertCollectionsSnapshotToMap} from "../../firebase/firebase.utils";


export const fetchCollectionsSuccess = (collectionsMap) => ({
    type : ShopActionTypes.FTECH_COLLECTIONS_SUCCESS,
    payload : collectionsMap,
});

export const fethCollectionsFailure = (errorMessage) => ({
    type : ShopActionTypes.FTECH_COLLECTIONS_FAILURE,
    payload: errorMessage
});

export const fetchCollectionsStart = () => ({
    type: ShopActionTypes.FTECH_COLLECTIONS_START
});


export const fetchCollectionsStartAsync = () => {
    return dispatch => {
        const collectionRef = firestore.collection('collections');
        dispatch(fetchCollectionsStart());
        collectionRef.get().then(snapshot => {
            const collectionsMap = convertCollectionsSnapshotToMap(snapshot);
            dispatch(fetchCollectionsSuccess(collectionsMap));
        }).catch(error => dispatch(fethCollectionsFailure(error.message)));
    }
};

