export const addItemToCart = (cartItems, cartItemToAdd) => {
    const existingCartItem = cartItems.find(cartItem => cartItem.id === cartItemToAdd.id);

    if (existingCartItem) {
      return cartItems.map(cartItem => cartItem.id === cartItemToAdd.id ? {
            ...cartItem,
            quantity: cartItem.quantity + 1
        } : cartItem);
    }

    return [...cartItems, {...cartItemToAdd, quantity: 1}]
};

export const removeItem = (cartItems, removeItem) => {
    const existingItem = cartItems.find(cartItem => cartItem.id === removeItem.id);

    if (existingItem.quantity === 1) {
        return cartItems.filter(cartItem => cartItem.id !== removeItem.id);
    }

    return cartItems.map(cartItem => cartItem.id === removeItem.id ? {...cartItem, quantity : cartItem.quantity - 1} : cartItem);

};