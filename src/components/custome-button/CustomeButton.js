import React from "react";
import "./CustomeButton.scss";

function CustomeButton({ children, isGoogleLogin, inverted, ...otherProps }) {
  return (
    <button className={`${inverted ? 'inverted' : '' } ${isGoogleLogin ? 'google-login' : '' } custom-button`} {...otherProps}>
      {children}
    </button>
  );
}

export default CustomeButton;
