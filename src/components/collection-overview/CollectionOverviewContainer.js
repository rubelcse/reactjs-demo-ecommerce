/**
 * Created by Ms.Ria (rubel mia) on 12/4/2020.
 */
import {connect} from 'react-redux';
import {compose} from 'redux';
import {createStructuredSelector} from 'reselect';
import {selectCollectionFetching} from '../../redux/shop/shop.selector';
import CollectionOverview from './CollectionOverview';
import WithSpinner from '../with-spinner/withSpinner';

const mapStateToProps = createStructuredSelector({
    isLoading: selectCollectionFetching
});

const CollectionOverviewContainer = compose(
    connect(mapStateToProps),
    WithSpinner
)(CollectionOverview);

export default CollectionOverviewContainer;
