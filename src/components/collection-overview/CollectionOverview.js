import React, {Fragment} from 'react';
import {createStructuredSelector} from 'reselect';
import {connect} from 'react-redux';
import { selectCollectionForPreview } from '../../redux/shop/shop.selector';
import CollectionPreview from '../collection-preview/CollectionPreview';

const CollectionOverview = ({collections}) => {
    return (
        <Fragment>
            <div className="collection-overview">
                {collections.map(({id, ...otherCollection}) => (
                    <CollectionPreview key={id} {...otherCollection} />
                ))}
            </div>
        </Fragment>
    )
};

const mapStateToProps = createStructuredSelector({
    collections : selectCollectionForPreview
});

export default connect(mapStateToProps, null) (CollectionOverview);