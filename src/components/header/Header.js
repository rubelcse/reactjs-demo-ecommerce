import React from "react";
import "./Header.scss";
import {ReactComponent as Logo} from "../../assets/crown.svg";
import {Link} from "react-router-dom";
import {auth} from '../../firebase/firebase.utils';
import {connect} from 'react-redux';
import CartIcon from '../cart-icon/CartIcon';
import CartDropdown from '../cart-dropdown/CartDropdown';
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from '../../redux/user/user.selector';
import { selectCartHidden } from '../../redux/cart/cart.selector';

function Header({currentUser, hidden}) {
    return (
        <div className='header'>
            <Link className='logo-container' to='/'>
                <Logo className='logo'/>
            </Link>
            <div className='options'>
                <Link className='option' to='/shop'>
                    SHOP
                </Link>
                <Link className='option' to='/contact'>
                    CONTACT
                </Link>
                {(currentUser && Object.keys(currentUser).length > 1) ? (
                    <div className="option" onClick={() => auth.signOut()}>SIGN OUT</div>) : (
                    <Link className="option" to='/signin'>SIGN IN</Link>)}
                <CartIcon/>
            </div>
            {hidden ? null : <CartDropdown/>}
        </div>
    );
}

const mapStateToProps = createStructuredSelector({ //automatically pass the state
    currentUser : selectCurrentUser,
    hidden : selectCartHidden
});

export default connect(mapStateToProps)(Header);
