import React, {Component} from "react";
import FormInput from "../form-input/FormInput";
import "./SignIn.scss";
import CustomeButton from "../custome-button/CustomeButton";
import {auth, signInWithGoogle} from '../../firebase/firebase.utils';

export default class SignIn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };
    }

    handleSubmit = event => {
        event.preventDefault();
        const { email, password } = this.state;
        try {
            auth.signInWithEmailAndPassword(email, password);
            this.setState({email: "", password: ""});
        }catch (e) {
            console.error(e);
        }
    };

    handleChange = event => {
        const {value, name} = event.target;
        this.setState({[name]: value});
    };

    render() {
        return (
            <div className='sign-in'>
                <h2>I already have an account</h2>
                <span>Sign in with your email and password</span>

                <form onSubmit={this.handleSubmit}>
                    <FormInput
                        name='email'
                        type='email'
                        label='email'
                        value={this.state.email}
                        required
                        handleChange={this.handleChange}
                    />
                    <FormInput
                        name='password'
                        type='password'
                        label='password'
                        value={this.state.password}
                        required
                        handleChange={this.handleChange}
                    />
                    <div className="buttons">
                        <CustomeButton style={{'marginRight' : '15px'}} type='submit'>Sign in</CustomeButton>
                        <CustomeButton onClick={signInWithGoogle} isGoogleLogin>Sign in With Google</CustomeButton>
                    </div>
                </form>
            </div>
        );
    }
}
