import React, {Fragment} from 'react';
import CustomButton from '../custome-button/CustomeButton';
import './CartDropdown.scss';
import CartItem from '../cart-item/CartItem';
import {connect} from 'react-redux';
import {selectCartItems} from '../../redux/cart/cart.selector';
import {createStructuredSelector} from 'reselect';
import { withRouter } from 'react-router-dom';
import { toggleCartHidden } from '../../redux/cart/cart.action';

const CartDropDown = ({cartItems, history, dispatch}) => {
    return (
        <Fragment>
            <div className="cart-dropdown">
                <div className="cart-items">
                    {cartItems.length ? cartItems.map(item => <CartItem key={item.id} item={item}/>) :
                        <span className="empty-message">Your cart is empty</span> }
                </div>
                <CustomButton onClick={() => {history.push('/checkout'); dispatch(toggleCartHidden())}}>GO TO CHECKOUT</CustomButton>
            </div>
        </Fragment>
    )
};

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems
});

export default withRouter(connect(mapStateToProps, null)(CartDropDown));