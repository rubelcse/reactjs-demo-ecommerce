import React from 'react';
import {ReactComponent as ShopingIcon} from '../../assets/shopping-bag.svg';
import './CartIcon.scss';
import {connect} from 'react-redux';
import {toggleCartHidden} from '../../redux/cart/cart.action';
import {selectCartItemsCount} from '../../redux/cart/cart.selector';
import { createStructuredSelector } from 'reselect';


const CartIcon = ({toggleCartHidden, itemCount}) => {
    return (
        <div className="cart-icon" onClick={toggleCartHidden}>
            <ShopingIcon className="shopping-icon"/>
            <span className="item-count">{itemCount}</span>
        </div>
    )
};


const mapStateToProps = createStructuredSelector({
    itemCount : selectCartItemsCount
});

const mapDispatchToProps = dispatch => ({
    toggleCartHidden : () => dispatch(toggleCartHidden())
});

export default connect(mapStateToProps, mapDispatchToProps) (CartIcon);


